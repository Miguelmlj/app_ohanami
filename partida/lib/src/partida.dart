import 'package:partida/src/puntuaciones.dart';

import '../partida.dart';
import 'helpers.dart';
import 'jugador.dart';
import 'problemas.dart';
import 'package:fpdart/fpdart.dart';

const numeroMaximoJugadores = 4;
const numeroMinimoJugadores = 2;
const maximoCartasJugadasRonda2 = 20;
const maximoCartasJugadasRonda3 = 30;
const puntosPorAzul = 3;
const puntosPorVerde = 4;
const puntosPorNegra = 7;
const puntosPorRosa = [
  0,1,3,6,10,15,21,28,36,45,55,66,78,91,105,120,
];

enum FasePuntuacion{ronda1,ronda2,ronda3, desenlace}

class Partida{
  Partida({required this.jugadores}){
  if(jugadores.length < numeroMinimoJugadores) throw ProblemaNumeroJugadoresMenorMinimo();
  if(jugadores.length > numeroMaximoJugadores)  throw ProblemaNumeroJugadoresMayorMaximo();

  }

  final Set<Jugador> jugadores;
  List<PRonda1> _puntuacionesRonda1 = [];
  List<PRonda2> _puntuacionesRonda2 = [];
  List<PRonda3> _puntuacionesRonda3 = [];

//  Future<Either<String,int>> obtenerNumero({required int minimo, required int maximo}) async {

  List<P> puntuaciones(FasePuntuacion ronda){
  //Either<List<PuntuacionJugador>,List<PuntuacionJugadorFinal>> puntuaciones(FasePuntuacion ronda){
  List<PuntuacionJugador> _puntuacionesJugador = [];
  List<PuntuacionJugadorFinal> _puntuacionesJugadorFinal = [];

    switch(ronda){
      case FasePuntuacion.ronda1:
      for (var pRonda1 in _puntuacionesRonda1) {
        Jugador jugador = pRonda1.jugador;
        int azules = pRonda1.cuantasAzules;
        _puntuacionesJugador.add(PuntuacionJugador(jugador:jugador,porAzules:puntosPorAzul*azules,porVerdes:0,porRosas:0,porNegras:0));
        //{required this.jugador, required this.porAzules, required this.porVerdes, required this.porRosas, required this.porNegras

        
      }
      //if(_puntuacionesJugador.isEmpty) throw ProblemaListaPuntuacionJugadorVacia();
      return _puntuacionesJugador;
     // return Left(_puntuacionesJugador);

      case FasePuntuacion.ronda2:
      for (var pRonda2 in _puntuacionesRonda2) {
        // Jugador jugador = pRonda2.jugador;
        // int azules = pRonda2.cuantasAzules;
        // int verdes = pRonda2.cuantasVerdes;
        _puntuacionesJugador.add(
          PuntuacionJugador(
            jugador:pRonda2.jugador,
            porAzules:pRonda2.cuantasAzules*puntosPorAzul,
            porVerdes:pRonda2.cuantasVerdes*puntosPorVerde,
            porRosas:0,
            porNegras:0));

        
      }
      return _puntuacionesJugador;
      // return Left(_puntuacionesJugador);


      case FasePuntuacion.ronda3:
      for (var pRonda3 in _puntuacionesRonda3) {
        _puntuacionesJugador.add(PuntuacionJugador(
          jugador: pRonda3.jugador,
          porAzules: pRonda3.cuantasAzules * puntosPorAzul,
          porVerdes: pRonda3.cuantasVerdes * puntosPorVerde,
          porNegras: pRonda3.cuantasNegras * puntosPorNegra,
          porRosas: pRonda3.cuantasRosas > 15 ? 120: puntosPorRosa[pRonda3.cuantasRosas]));
      }
      return _puntuacionesJugador;
      // return Left(_puntuacionesJugador);


      case FasePuntuacion.desenlace:
        
        for (var i = 0; i < _puntuacionesRonda1.length; i++) {
          PRonda1 pr1 = _puntuacionesRonda1[i];
          PRonda2 pr2 = _puntuacionesRonda2[i];
          PRonda3 pr3 = _puntuacionesRonda3[i];
          Jugador j = _puntuacionesRonda1[i].jugador;
          int total = 0;

          total = pr1.cuantasAzules * puntosPorAzul+
                  pr2.cuantasAzules * puntosPorAzul+
                  pr2.cuantasVerdes * puntosPorVerde+
                  pr3.cuantasAzules * puntosPorAzul+
                  pr3.cuantasVerdes * puntosPorVerde+
                  pr3.cuantasNegras * puntosPorNegra+
                  (_puntuacionesRonda3[i].cuantasRosas > 15 ? 120: puntosPorRosa[pr3.cuantasRosas]);

                  _puntuacionesJugadorFinal.add(PuntuacionJugadorFinal(jugador: j,total: total));
        
        }

       print('${_puntuacionesJugadorFinal[0].jugador.nombre} : ${_puntuacionesJugadorFinal[0].total}');

       print('${_puntuacionesJugadorFinal[1].jugador.nombre} : ${_puntuacionesJugadorFinal[1].total}');
        
        return _puntuacionesJugadorFinal;
      // return Right(_puntuacionesJugadorFinal);

    }
    
  }

  //====PUNTUACION RONDA 1
  void puntuacionRonda1(List<PRonda1> puntuaciones){
    Set<Jugador> jugadoresR1 = puntuaciones.map((e) => e.jugador).toSet();
    if(!setEquals(jugadores,jugadoresR1)) throw ProblemaJugadoresNoConcuerdan();
    
    _puntuacionesRonda1 = puntuaciones;

  }

  
  //====PUNTUACION RONDA 2
  void puntuacionRonda2(List<PRonda2> puntuaciones){
    if(_puntuacionesRonda1.isEmpty) throw ProblemaOrdenIncorrecto();
    
    Set<Jugador> jugadoresR2 = puntuaciones.map((e) => e.jugador).toSet();
    if(!setEquals(jugadores,jugadoresR2)) throw ProblemaJugadoresNoConcuerdan();

    for(PRonda2 segundaPuntuacion in puntuaciones){
      PRonda1 primeraPuntuacion = _puntuacionesRonda1.firstWhere((element) => element.jugador ==segundaPuntuacion.jugador);
      if(primeraPuntuacion.cuantasAzules > segundaPuntuacion.cuantasAzules) {
        throw ProblemaDisminucionAzules();
      }

    for(PRonda2 p in puntuaciones){
      if(p.cuantasAzules >maximoCartasJugadasRonda2) throw ProblemaDemasiadasAzules();
       if(p.cuantasVerdes>maximoCartasJugadasRonda2) throw ProblemaDemasiadasVerdes();
       if((p.cuantasVerdes + p.cuantasVerdes) > maximoCartasJugadasRonda2) throw ProblemaExcesoCartas();
    }

    }

    _puntuacionesRonda2 = puntuaciones;
    //print(_puntuacionesRonda2[1].jugador.nombre);
  }

  //====PUNTUACION RONDA 3
  void puntuacionRonda3(List<PRonda3> puntuaciones){
    if(_puntuacionesRonda2.isEmpty) throw ProblemaOrdenIncorrecto();

    Set<Jugador> jugadoresR3 = puntuaciones.map((e) => e.jugador).toSet();
    if(!setEquals(jugadores, jugadoresR3)) throw ProblemaJugadoresNoConcuerdan();

    for (PRonda3 terceraPuntuacion in puntuaciones) {
      PRonda2 segundaPuntuacion = _puntuacionesRonda2.firstWhere((element) => element.jugador == terceraPuntuacion.jugador);

      if(segundaPuntuacion.cuantasAzules > terceraPuntuacion.cuantasAzules) throw ProblemaDisminucionAzules();
      if(segundaPuntuacion.cuantasVerdes > terceraPuntuacion.cuantasVerdes) throw ProblemaDisminucionVerdes();

      for (PRonda3 p in puntuaciones) {
        if(p.cuantasAzules > maximoCartasJugadasRonda3) throw ProblemaDemasiadasAzules();
        if(p.cuantasVerdes > maximoCartasJugadasRonda3) throw ProblemaDemasiadasVerdes();
        if(p.cuantasNegras > maximoCartasJugadasRonda3) throw ProblemaDemasiadasNegras();
        if(p.cuantasRosas > maximoCartasJugadasRonda3)  throw ProblemaDemasiadasRosas();
        if((p.cuantasVerdes + p.cuantasAzules + p.cuantasNegras + p.cuantasRosas) > maximoCartasJugadasRonda3) throw ProblemaExcesoCartas();
      }
      
    }

    _puntuacionesRonda3 = puntuaciones;

  }

}



