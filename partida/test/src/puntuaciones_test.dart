import 'package:partida/partida.dart';
import 'package:partida/src/jugador.dart';
import 'package:test/test.dart';

void main() {
  
  group('Puntuacion Ronda 1', (){
    late Jugador jugador;
    setUp((){
      jugador = Jugador(nombre: 'x');
    });

    test('La cantidad de azules debe ser cero o mayor', (){
      expect(()=>PRonda1(jugador: jugador, cuantasAzules: -1), throwsA(TypeMatcher<ProblemaAzulesNegativas>()));
    });

    test('Máximo azules es 10', (){
      expect(()=>PRonda1(jugador: jugador,cuantasAzules: 11), throwsA(TypeMatcher<ProblemaDemasiadasAzules>()));
    });

    test('Cantidad entre 1 y 10 esta bien', (){
      expect(()=>PRonda1(jugador: jugador,cuantasAzules: 3), returnsNormally);
    });

    // test('Cartas azules no deben ser negativas', (){
    //   expect(()=>PRonda2(jugador: jugador, cuantasAzules:-1,cuantasVerdes:0), throwsA(TypeMatcher<ProblemaAzulesNegativas>()));
    // });

    // test('Cartas verdes deben ser positivas', (){
    //   expect(()=>PRonda2(jugador: jugador, cuantasAzules:3,cuantasVerdes:0), returnsNormally);
    // });

    // test('Cartas verdes positivas', (){});
  });

  group('Puntuaciones Ronda 2', (){
    late Jugador jugador;
    setUp((){
      jugador = Jugador(nombre: 'Pancho');
    });

    test('Cartas azules no deben ser negativas', (){
      expect(()=>PRonda2(jugador: jugador, cuantasAzules: -1, cuantasVerdes: 0), throwsA(TypeMatcher<ProblemaAzulesNegativas>()));
    });

    test('se aceptan numeros positivos de cartas azules', (){
      expect(()=>PRonda2(jugador: jugador, cuantasAzules: 3, cuantasVerdes: 3), returnsNormally);
    });

    test('Cartas verdes deben ser positivas', (){
        expect(()=>PRonda2(jugador: jugador, cuantasAzules: 3, cuantasVerdes:0), returnsNormally);
    });

    test('Cartas verdes no deben ser negativas', (){
        expect(()=>PRonda2(jugador: jugador, cuantasAzules: 0, cuantasVerdes:-1), throwsA(TypeMatcher<ProblemaVerdesNegativas>()));
    });
  });

  group('Puntuaciones Ronda 3', (){

      late Jugador jugador;
      setUp((){
        jugador = Jugador(nombre: 'Name');
      });

      test('La cantidad de azules debe ser cero o +', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:-1, cuantasVerdes:0, cuantasNegras: 0, cuantasRosas: 0),throwsA(TypeMatcher<ProblemaAzulesNegativas>()));
      });

      test('Se aceptan números positivos de cartas azules', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:3, cuantasVerdes: 0, cuantasNegras:0, cuantasRosas:0), returnsNormally);
      });

      test('La cantidad de verdes debe ser cero o +', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:0, cuantasVerdes:-1, cuantasNegras: 0, cuantasRosas: 0),throwsA(TypeMatcher<ProblemaVerdesNegativas>()));
      });

      test('Se aceptan números positivos de cartas verdes', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:0, cuantasVerdes: 3, cuantasNegras:0, cuantasRosas:0), returnsNormally);
      });

      test('La cantidad de negras debe ser cero o +', () {
        expect(()=>PRonda3(jugador: jugador,cuantasAzules:0,cuantasVerdes:0,cuantasNegras:-1,cuantasRosas:0), throwsA(TypeMatcher<ProblemaNegrasNegativas>()));
      });

      test('Se aceptan números positivos de cartas Negras', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:0, cuantasVerdes: 0, cuantasNegras:3, cuantasRosas:0), returnsNormally);
      });

      test('La cantidad de rosas debe ser cero o +', () {
        expect(()=>PRonda3(jugador: jugador,cuantasAzules:0,cuantasVerdes:0,cuantasNegras:0,cuantasRosas:-1), throwsA(TypeMatcher<ProblemaRosasNegativas>()));
      });

      test('Se aceptan números positivos de cartas rosas', () {
        expect(()=>PRonda3(jugador: jugador, cuantasAzules:0, cuantasVerdes: 0, cuantasNegras:0, cuantasRosas:3), returnsNormally);
      });  

    });


}

//pruebas Ronda3
//Numero de azules 0 o mas
//numero de verdes 0 o mas
//numero de negras 0 o mas
//numeros de rosas 0 o mas