import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/bloc/estados.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/lista_partidas/eliminar_partida.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/lista_partidas/resp_eliminar_partida.dart';

import 'package:p_ohanami/vistas/vistas_export.dart';

class VistaPantallas extends StatelessWidget {
  const VistaPantallas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocUno>().state;

    if (estado is Login) {
      return PantallaLogin();
    }

    if (estado is Iniciandome) {
      return PantallaIniciandoSesion(estado.nombre, estado.contrasena);
    }

    if (estado is PaginaPrincipal) {
      return PantallaPrincipal();
    }

    if (estado is PaginaRegistro) {
      return PantallaRegistro();
    }

    if (estado is Registrandome) {
      return PantallaCargandoNuevoRegistro(estado.nombre, estado.contrasena);
    }

    if (estado is PaginaErrorAcceso) {
      return PantallaErrorAcceso(estado.mensaje);
    }
    if (estado is PaginaRespRegistro) {
      return PantallaRespRegistro(estado.mensajeRegistro);
    }
    if (estado is PaginaNuevaPartida) {
      return PantallaSeleccionJugadores();
    }
    if (estado is PaginaMisPartidas) {
      return PantallaListaMisPartidas(estado.misPartidas);
    }
    if (estado is PaginaNombrarJugadores) {
      return PantallaNombrar(estado.cantidadJugadores, estado.nombreJugadores);
    }
    if (estado is ErrorNombresJugadores) {
      return PantallaErrorNombres(estado.mensajeValidacion);
    }
    if (estado is PaginaRondaPrimera) {
      return PantallaRonda1(estado.listaConJugadores, estado.ronda);
    }
    if (estado is PaginaCartasJugadorR1) {
      return PuntuacionCartasR1(estado.ronda);
    }
    if (estado is ErrorJugadorSeleccionado) {
      return PantallaErrorJugadorPuntuado(estado.msj);
    }
    if (estado is VerDetallesPartida) {
      return PantallaResultadosPartida();
    }
    if (estado is CargandoPartidaTerminada) {
      return PantallaFinalizandoPartida();
    }

    if (estado is PartidaTerminada) {
      return PantallaResultadoPartidaTerminada();
    }
    if (estado is EstadisticaJugadorMostrada) {
      return PantallaGraficaRondas(estado.lista, estado.indice, estado.index);
    }
    if (estado is ErrorConexionPartidaT) {
      return PantallaErrActualizarPartidas(estado.msj, estado.msj2);
    }
    if (estado is EliminandoPartida) {
      return PantallaEliminandoPartida(estado.indice);
    }
    if (estado is PartidaEliminada) {
      return RespEliminarPartida(estado.mensaje);
    } else {
      return Container();
    }
  }
}
