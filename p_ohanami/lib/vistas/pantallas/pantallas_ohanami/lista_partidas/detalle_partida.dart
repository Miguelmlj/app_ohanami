import 'package:flutter/material.dart';
import 'package:p_ohanami/app/estilos.dart';
import 'package:p_ohanami/bloc/bloc.dart';
import 'package:p_ohanami/bloc/estados.dart';
import 'package:p_ohanami/bloc/eventos.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PantallaResultadosPartida extends StatelessWidget {
  const PantallaResultadosPartida({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Detalles de Partida')),
          backgroundColor: Colors.red.shade300),
      body: const CuerpoDetallesPartida(),
    );
  }
}

class CuerpoDetallesPartida extends StatelessWidget {
  const CuerpoDetallesPartida({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocUno>().state;

    List<dynamic> partida = (estado as VerDetallesPartida).detallePartida;

    int indice = (estado as VerDetallesPartida).index;

    int totalJugadores = partida[indice]['jugadores'].length;
    String winner = "Empate";
    int puntuacionA = 0;
    for (var i = 0; i < totalJugadores; i++) {
      if (partida[indice]['jugadores'][i]['puntuacionFinal'] > puntuacionA) {
        puntuacionA = partida[indice]['jugadores'][i]['puntuacionFinal'];
        winner = partida[indice]['jugadores'][i]['nombreJugador'];
      }
    }

    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Fecha de Partida: ${partida[indice]['fecha']}', style: estiloMensaje,),
        ),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Ganador: ', style: estiloMensaje,),
            Text('$winner', style: estiloJugGan,),
          ],
        ),
        Divider(),
        Text('Resultados: ', style: estiloMensaje,),
        Divider(),
        Card(

          color: Colors.black54,
          elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text('Jugador', style: estiloBarPartida,),
                  //SizedBox(width: 70.0,),
                  Text('Puntuacion', style: estiloBarPartida,),
                  //SizedBox(width: 70.0,),
                  Text('Gráfica', style: estiloBarPartida,),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 10.0,),
        Expanded(
            child: ListView.builder(
                itemCount: partida[indice]['jugadores'].length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Divider(),
                            Text(
                                '${partida[indice]['jugadores'][index]['nombreJugador']}', style: estiloJugPart,),
                            Divider(),
                            Text(
                                '${partida[indice]['jugadores'][index]['puntuacionFinal']}', style: estiloJugPunt,),
                            Divider(),
                            IconButton(
                                onPressed: () {
                                  context.read<BlocUno>().add(
                                      VerEstadisticaJugador(
                                          lista: partida,
                                          indice: indice,
                                          index: index));
                                },
                                icon: Icon(Icons.arrow_forward))
                          ],
                        )
                      ],
                    ),
                  );
                })),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
               style: ElevatedButton.styleFrom(
                    primary: Colors.red.shade300,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
              onPressed: () {
                context.read<BlocUno>().add(SalirDePartida());
              },
              child: Text('Menú Principal'),
            ),
            ElevatedButton(
               style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
              onPressed: () {
                context.read<BlocUno>().add(AbrirListaPartidas());
              },
              child: Text('Lista de Partidas'),
            ),
          ],
        )
      ],
    );
  }
}
