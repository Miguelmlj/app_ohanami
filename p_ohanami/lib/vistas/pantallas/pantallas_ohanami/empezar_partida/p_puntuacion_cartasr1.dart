import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PuntuacionCartasR1 extends StatelessWidget {
  const PuntuacionCartasR1(this.ronda,{Key? key}) : super(key: key);

  final int ronda;

  @override
  Widget build(BuildContext context) {
    

    var estiloColorAppBar = Colors.black54;
    if (ronda == 1) {
      estiloColorAppBar = Colors.black54;
    }
    if (ronda == 2) {
      estiloColorAppBar = Colors.red.shade300;
    }
    if (ronda == 3) {
      estiloColorAppBar = Colors.blueGrey;
    }

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Cantidad de Cartas Ronda $ronda')),
        backgroundColor: estiloColorAppBar,
      ),
      body: CantidadDeCartasR1(),
     
    );
  }
}

class CantidadDeCartasR1 extends StatefulWidget {
  const CantidadDeCartasR1({Key? key}) : super(key: key);
  

  @override
  _CantidadDeCartasR1State createState() => _CantidadDeCartasR1State();
}

class _CantidadDeCartasR1State extends State<CantidadDeCartasR1> {
   
  //total de cartas a repartir
  int cartasJugador = 0;
  List<int> numerosDeCartas = [];

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocUno>().state;
    final String nombreJugador =
        (estado as PaginaCartasJugadorR1).nombreJugador;
    final List<int> listaConCartas =
        (estado as PaginaCartasJugadorR1).numeroDeCartas;

    final int cantidadCartasRepartidas =
        (estado as PaginaCartasJugadorR1).cantidadCartasRepartir;

    cartasJugador = cantidadCartasRepartidas;
    numerosDeCartas = listaConCartas;

    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Numerar cartas para jugador: $nombreJugador", style: estiloMensaje,),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Total de cartas: $cartasJugador"),
        ),
        SizedBox(height: 30.0,),
        Expanded(
            child: ListView.builder(
          itemCount: numerosDeCartas.length,
          itemBuilder: (BuildContext context, int index) {
            return new Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            if ((index == azules) &&
                                (cartasJugador != loteLleno &&
                                    numerosDeCartas[index] != loteVacio)) {
                              numerosDeCartas[index]--;
                              cartasJugador++;
                            } else if ((index == verdes) &&
                                (cartasJugador != loteLleno &&
                                    numerosDeCartas[index] != loteVacio)) {
                              numerosDeCartas[index]--;
                              cartasJugador++;
                            } else if ((index == rosas) &&
                                (cartasJugador != loteLleno &&
                                    numerosDeCartas[index] != loteVacio)) {
                              numerosDeCartas[index]--;
                              cartasJugador++;
                            } else if ((index == negras) &&
                                (cartasJugador != loteLleno &&
                                    numerosDeCartas[index] != loteVacio)) {
                              numerosDeCartas[index]--;
                              cartasJugador++;
                            }

                            //imprimir esto
                            for (var i = 0; i < numerosDeCartas.length; i++) {
                              print(numerosDeCartas);
                            }

                            context.read<BlocUno>().add(EditandoNumeroCartasr1(
                                cantidadDeCartas: numerosDeCartas,
                                totalCartasRepartidas: cartasJugador));
                          });
                        },
                        icon: Icon(Icons.arrow_back_ios)),
                    index == 0
                        ? Row(
                            children: [
                              Icon(
                                Icons.calculate_rounded,
                                size: 50.0,
                                color: Colors.blue,
                              ),
                              Text('Azules: ${numerosDeCartas[index]}', style: estiloMensaje,),
                            ],
                          )
                        : Text(''),
                    index == 1
                        ? Row(
                            children: [
                              Icon(
                                Icons.calculate_rounded,
                                size: 50.0,
                                color: Colors.green,
                              ),
                              Text('Verdes: ${numerosDeCartas[index]}', style: estiloMensaje,),
                            ],
                          )
                        : Text(''),
                    index == 2
                        ? Row(
                            children: [
                              Icon(
                                Icons.calculate_rounded,
                                size: 50.0,
                                color: Colors.black54,
                              ),
                              Text('Negras: ${numerosDeCartas[index]}', style: estiloMensaje,),
                            ],
                          )
                        : Text(''),
                    index == 3
                        ? Row(
                            children: [
                              Icon(
                                Icons.calculate_rounded,
                                size: 50.0,
                                color: Colors.pink.shade300,
                              ),
                              Text('Rosas: ${numerosDeCartas[index]}', style: estiloMensaje,),
                            ],
                          )
                        : Text(''),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            if ((index == azules) &&
                                (cartasJugador != loteVacio &&
                                    numerosDeCartas[index] != loteLleno)) {
                              numerosDeCartas[index]++;
                              cartasJugador--;
                            } else if ((index == verdes) &&
                                (cartasJugador != loteVacio &&
                                    numerosDeCartas[index] != loteLleno)) {
                              cartasJugador--;
                              numerosDeCartas[index]++;
                            } else if ((index == rosas) &&
                                (cartasJugador != loteVacio &&
                                    numerosDeCartas[index] != loteLleno)) {
                              cartasJugador--;
                              numerosDeCartas[index]++;
                            } else if ((index == negras) &&
                                (cartasJugador != loteVacio &&
                                    numerosDeCartas[index] != loteLleno)) {
                              cartasJugador--;
                              numerosDeCartas[index]++;
                            }

                            //imprimir esto
                            for (var i = 0; i < numerosDeCartas.length; i++) {
                              print(numerosDeCartas);
                            }

                            context.read<BlocUno>().add(EditandoNumeroCartasr1(
                                cantidadDeCartas: numerosDeCartas,
                                totalCartasRepartidas: cartasJugador));
                          });
                        },
                        icon: Icon(Icons.arrow_forward_ios_outlined)),
                  ],
                ),

                Divider(),
              ],
            );
          },
        )),
        Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.black54,
                  onPrimary: Colors.white,
                  shadowColor: Colors.teal,
                  elevation: 5,
                  textStyle: const TextStyle(
                    fontSize: 20,
                    fontStyle: FontStyle.italic,
                  )),
              onPressed: () {
                context
                    .read<BlocUno>()
                    .add(RegresarAJugadoresAPuntuar(puntuado: false));
              },
              child: Text("Aceptar"),
            )),
      ],
    );
  }
}
