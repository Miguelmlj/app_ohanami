import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaErrActualizarPartidas extends StatelessWidget {
  const PantallaErrActualizarPartidas(this.msj1, this.msj2,{ Key? key }) : super(key: key);

  final String msj1;
  final String msj2;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Error Cargar Partida')),
        backgroundColor: Colors.black54,
      ),
      body: ErrSincronizarPartida(msj1, msj2),
    );
  }
}


class ErrSincronizarPartida extends StatelessWidget {
  const ErrSincronizarPartida(this.msj1, this.msj2,{ Key? key }) : super(key: key);

  final String msj1;
  final String msj2;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text('$msj1. $msj2',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: const Color(0xFF616161))),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Icon(Icons.error_outline, size: 70, color: Colors.red.shade300),
            SizedBox(
              height: 100.0,
            ),
            ElevatedButton(
              style:  ElevatedButton.styleFrom(
                primary: Colors.red.shade300,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),

              onPressed: () {
                 context.read<BlocUno>().add(SalirDePartida());
              },
              child: Text('Menú Principal'),
            )
          ],
        ),
      );
  }
}