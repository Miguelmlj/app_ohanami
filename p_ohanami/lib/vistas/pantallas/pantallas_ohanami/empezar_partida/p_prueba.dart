import 'package:flutter/material.dart';

import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaPrueba extends StatelessWidget {
  const PantallaPrueba(this.num,{ Key? key }) : super(key: key);

  final int num;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Center(child: Text('Prueba')),backgroundColor: Colors.black54),
      body: CuerpoPrueba(num),  
      
    );
  }
}


class CuerpoPrueba extends StatefulWidget {
  const CuerpoPrueba(this.num,{ Key? key }) : super(key: key);
  final int num;


  @override
  _CuerpoPruebaState createState() => _CuerpoPruebaState(num);
}

class _CuerpoPruebaState extends State<CuerpoPrueba> {

  int num;

  _CuerpoPruebaState(this.num);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text('s: $num'),),
      
    );
  }
}