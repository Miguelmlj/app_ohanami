import 'package:flutter/material.dart';

import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaErrorJugadorPuntuado extends StatelessWidget {
  const PantallaErrorJugadorPuntuado(this.mensaje,{Key? key}) : super(key: key);
  final String mensaje;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Mensaje Puntuaciones')),
        backgroundColor: Colors.black54,
      ),
      body: CuerpoJugadorPuntuado(mensaje),
    );
  }
}

class CuerpoJugadorPuntuado extends StatelessWidget {
  const CuerpoJugadorPuntuado(this.mensaje,{ Key? key }) : super(key: key);
final String mensaje;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            SizedBox(height: 50),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(mensaje,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: const Color(0xFF616161))),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Icon(Icons.sports_esports_outlined, size: 70, color: Colors.green),
            SizedBox(
              height: 100.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
              onPressed: () {
                context
                    .read<BlocUno>()
                    .add(RegresarAJugadoresAPuntuar(puntuado: true));
              },
              child: Text('Regresar'),
            )
          ],
        ),
      );
  }
}
