import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaSeleccionJugadores extends StatelessWidget {
  const PantallaSeleccionJugadores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Seleccionar jugadores')),
        backgroundColor: Colors.black54,
      ),
      body: const CuerpoSeleccionJugadores(),
      bottomNavigationBar: OpcionInferior(),
    );
  }
}

class OpcionInferior extends StatelessWidget {
  const OpcionInferior({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade300,
                  onPrimary: Colors.white,
                  shadowColor: Colors.teal,
                  elevation: 5,
                  textStyle: const TextStyle(
                    fontSize: 12,
                    fontStyle: FontStyle.italic,
                  )),
              onPressed: () => _mostrarAdvertencia(context),
              onLongPress: () {
                context.read<BlocUno>().add(SalirDePartida());
              },
              child: Text('Ir a inicio'),
            ),
          ),
          SizedBox(
            width: 190.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () {
                  context.read<BlocUno>().add(AbrirNombrarJugadores());
                },
                child: Text('Siguiente')),
          )
        ],
      ),
    );
  }

  void _mostrarAdvertencia(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('Advertencia'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(salirDePartida),
                iconoAdvertencia,
              ],
            ),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.black54,
                    onPrimary: Colors.white,
                    shadowColor: Colors.teal,
                    elevation: 5,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Aceptar'),
              ),
            ],
          );
        });
  }
}

class CuerpoSeleccionJugadores extends StatelessWidget {
  const CuerpoSeleccionJugadores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: [
        Center(
          child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              margin: EdgeInsets.all(15),
              elevation: 10,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Column(
                  children: <Widget>[
                    FadeInImage(
                      image: AssetImage(imagenJugadores),
                      placeholder: AssetImage(imagenLoading),
                      fit: BoxFit.cover,
                      height: 150,
                    ),
                    //
                  ],
                ),
              )),
        ),
        Center(
            child: Text('Seleccionar cantidad de jugadores',
                style: estiloMensaje)),
        CantidadJugadores(),
      ],
    ));
  }
}

class CantidadJugadores extends StatefulWidget {
  const CantidadJugadores({Key? key}) : super(key: key);

  @override
  _CantidadJugadoresState createState() => _CantidadJugadoresState();
}

class _CantidadJugadoresState extends State<CantidadJugadores> {
  late String vista = "Cantidad de Jugadores";

  late int opcionSeleccionada;

  final List<String> listaJugadores = [
    '2 Jugadores',
    '3 Jugadores',
    '4 Jugadores',
  ];

  @override
  void initState() {
    super.initState();
    opcionSeleccionada = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        ...listaJugadores
            .asMap()
            .entries
            .map((e) => Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ChoiceChip(
                      elevation: 5,
                      shadowColor: Colors.teal,
                      backgroundColor: Colors.red.shade300,
                      selectedColor: Colors.black54,
                      labelStyle: TextStyle(color: Colors.white),
                      label: Text(e.value),
                      selected: opcionSeleccionada == e.key,
                      onSelected: (seleccionado) {
                        setState(() {
                          opcionSeleccionada = e.key;
                          print(opcionSeleccionada);

                          context.read<BlocUno>().add(CantidadDeJugadores(
                              cantidadJugadores: opcionSeleccionada));
                        });
                      },
                    ),
                  ),
                ))
            .toList(),
      ],
    );
  }
}
