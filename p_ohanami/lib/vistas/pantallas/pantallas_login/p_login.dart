import 'package:flutter/material.dart';
import 'package:p_ohanami/vistas/pantallas/pantallas_ohanami/p_export.dart';

class PantallaLogin extends StatelessWidget {
  const PantallaLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text('Login')), backgroundColor: Colors.black54),
      body: CuerpoLogin(),
    );
  }
}

class CuerpoLogin extends StatelessWidget {
  const CuerpoLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Contenedor();
  }
}

class Contenedor extends StatefulWidget {
  const Contenedor({Key? key}) : super(key: key);

  @override
  _ContenedorState createState() => _ContenedorState();
}

class _ContenedorState extends State<Contenedor> {
  String nombre = "";
  String contrasena = "";

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      children: [
        Column(
          children: [
            Center(
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  margin: EdgeInsets.all(15),
                  elevation: 10,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: Column(
                      children: <Widget>[
                        FadeInImage(
                          image: AssetImage(imagenInicioSesion),
                          placeholder: AssetImage(imagenLoading),
                          fit: BoxFit.cover,
                          height: 130,
                        ),
                        //
                      ],
                    ),
                  )),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Iniciar Sesión',
                    style: const TextStyle(
                        //fontWeight: FontWeight.bold,
                        fontSize: 30.0,
                        color: const Color(0xFF616161))),
              ),
            )
          ],
        ),

        //CAMPO INPUT NOMBRE
        TextField(
          //autofocus: true,
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
              hintText: 'Nombre',
              labelText: 'Nombre',
              helperText: 'Escriba su nombre de usuario para acceder',
              suffixIcon: Icon(Icons.accessibility),
              icon: Icon(Icons.account_circle)),
          onChanged: (valor) {
            setState(() {
              nombre = valor;
            });
          },
        ),
        TextField(
            obscureText: true,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                hintText: 'Contraseña',
                labelText: 'Contraseña',
                helperText: 'Ingrese una contraseña',
                suffixIcon: Icon(Icons.lock_open),
                icon: Icon(Icons.lock)),
            onChanged: (valor) => setState(() {
                  contrasena = valor;
                })),

        SizedBox(height: 20),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.red.shade300,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),
            onPressed: () {
              context.read<BlocUno>().add(CargandoIniciandoSesion(
                  nombre: nombre, contrasena: contrasena));
            },
            child: const Text('Acceder'),
          ),
        ),

        SizedBox(height: 20),
        Center(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.black54,
                onPrimary: Colors.white,
                shadowColor: Colors.teal,
                elevation: 5,
                textStyle: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                )),
            onPressed: () {
              context.read<BlocUno>().add(AbrirRegistro());
            },
            child: const Text('Registrarme'),
          ),
        ),
      ],
    );
  }
}
