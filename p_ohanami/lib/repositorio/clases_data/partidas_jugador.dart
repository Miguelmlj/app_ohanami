import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:p_ohanami/repositorio/clases_data/exp_clases.dart';

class PartidasJugador {
  String fecha;
  List<DetallesJugador> jugadores;
  PartidasJugador({
    required this.fecha,
    required this.jugadores,
  });
  

  PartidasJugador copyWith({
    String? fecha,
    List<DetallesJugador>? jugadores,
  }) {
    return PartidasJugador(
      fecha: fecha ?? this.fecha,
      jugadores: jugadores ?? this.jugadores,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'fecha': fecha,
      'jugadores': jugadores.map((x) => x.toMap()).toList(),
    };
  }

  factory PartidasJugador.fromMap(Map<String, dynamic> map) {
    return PartidasJugador(
      fecha: map['fecha'] ?? '',
      jugadores: List<DetallesJugador>.from(map['jugadores']?.map((x) => DetallesJugador.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory PartidasJugador.fromJson(String source) => PartidasJugador.fromMap(json.decode(source));

  @override
  String toString() => 'PartidasJugador(fecha: $fecha, jugadores: $jugadores)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is PartidasJugador &&
      other.fecha == fecha &&
      listEquals(other.jugadores, jugadores);
  }

  @override
  int get hashCode => fecha.hashCode ^ jugadores.hashCode;
}
