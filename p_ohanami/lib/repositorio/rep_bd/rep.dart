import 'package:partida/partida.dart';

abstract class Rep {
  Future<String> registradoUsuario({required String nombre, required String contrasena});

  Future<String> descargarData({required String nombre});

  Map<String, dynamic> obtenerPartidas();

  Future<String> registrarUsuario({required String nombre, required String contrasena});

  String registrarJugadoresPartida({required List<String> nombres});

  void registrarPuntuacionesR1({required List<PRonda3> jugadoresConCartas});
  void registrarPuntuacionesR2({required List<PRonda3> jugadoresConCartas});
  void registrarPuntuacionesR3({required List<PRonda3> jugadoresConCartas});

  Future<String> registrarPuntuacionFinal();

  void salirDePartida();

  Future<String> subirPartida({required String usuario});

  Future<String> eliminarPartida({required int indice, required String usuario});

  void cerrarSesion();
}
