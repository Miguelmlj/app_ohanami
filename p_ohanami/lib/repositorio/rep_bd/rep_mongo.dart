import 'package:p_ohanami/repositorio/clases_data/exp_clases.dart';
import 'package:p_ohanami/repositorio/rep_bd/rep.dart';
import 'dart:convert';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:p_ohanami/app/constantes.dart';
import 'package:partida/partida.dart';
import 'package:intl/intl.dart';

class RepMongo extends Rep {
  Map<String, dynamic> dataUsuario = {};

  List<Jugador> _nombresJugadores = [];
  Set<Jugador> _jugadores = {};
  late Partida partida;

  late PRonda3 datosJugador;

  List<PRonda3> datosJugadoresr1 = [];
  List<PRonda3> datosJugadoresr2 = [];
  List<PRonda3> datosJugadoresr3 = [];

  List<PRonda1> dtsJugadoresr1 = [];
  List<PRonda2> dtsJugadoresr2 = [];
  List<PRonda3> dtsJugadoresr3 = [];

  List<PuntuacionJugadorFinal> puntuacionesJugadorFinal = [];

  List<DetallesJugador> detallesJugador = [];

  List<int> cartasRonda1 = [];

  @override
  Future<String> registrarUsuario(
      {required String nombre, required String contrasena}) async {
    if (nombre == "" || contrasena == "") {
      return credencialesVacias;
    }

    try {
      var db = await Db.create(conexionServidor);

      await db.open(secure: true);

      var col = db.collection('usuarios');

      var buscarUsuaro = await col.findOne(where.eq('nombre', nombre));

      if (buscarUsuaro == null) {
        Usuarios registrarusuario =
            Usuarios(nombre: nombre, contrasena: contrasena, partidas: []);

        var result = await col.insert(jsonDecode(registrarusuario.toJson()));

        await db.close();

        return mensajeRegistro1;
      } else {
        return mensajeRegistro2;
      }
    } catch (e) {
      return mensajeRegistro3;
    }
  }

  @override
  Future<String> registradoUsuario(
      {required String nombre, required String contrasena}) async {
    try {
      var db = await Db.create(conexionServidor);

      await db.open(secure: true);
      var col = db.collection('usuarios');
      var result = await col.findOne(where.eq('nombre', nombre));

      await db.close();

      if (result == null) {
        return mensajeAccesoLogin2;
      } else {
        if (result['contrasena'] != contrasena) {
          return mensajeAccesoLogin4;
        }
        return mensajeAccesoLogin1;
      }
    } catch (e) {
      return mensajeAccesoLogin3;
    }
  }

  @override
  String registrarJugadoresPartida({required List<String> nombres}) {
    Jugador j1;
    Jugador j2;
    Jugador j3;
    Jugador j4;
    _nombresJugadores = [];
    _jugadores = {};

    var jugadoresRepetidos = nombres.toSet().toList();

    if (jugadoresRepetidos.length != nombres.length) {
      return nombresRepetidos;
    } else {
      for (var i = 0; i < nombres.length; i++) {
        try {
          if (i == 0) {
            j1 = Jugador(nombre: nombres[i]);
            _nombresJugadores.add(j1);
          } else if (i == 1) {
            j2 = Jugador(nombre: nombres[i]);
            _nombresJugadores.add(j2);
          } else if (i == 2) {
            j3 = Jugador(nombre: nombres[i]);
            _nombresJugadores.add(j3);
          } else if (i == 3) {
            j4 = Jugador(nombre: nombres[i]);
            _nombresJugadores.add(j4);
          }
        } on ProblemaNombreJugadorVacio {
          return camposVacios;
        }
      }

      return nombresCorrectos;
    }
  }

  @override
  void salirDePartida() {
    _nombresJugadores = [];
    _jugadores = {};
    datosJugadoresr1 = [];
    datosJugadoresr2 = [];
    datosJugadoresr3 = [];

    dtsJugadoresr1 = [];
    dtsJugadoresr2 = [];
    dtsJugadoresr3 = [];

    puntuacionesJugadorFinal = [];
    detallesJugador = [];
    cartasRonda1 = [];
  }

  @override
  void registrarPuntuacionesR1({required List<PRonda3> jugadoresConCartas}) {
    datosJugadoresr1 = jugadoresConCartas;
  }

  @override
  void registrarPuntuacionesR2({required List<PRonda3> jugadoresConCartas}) {
    datosJugadoresr2 = jugadoresConCartas;
  }

  @override
  void registrarPuntuacionesR3({required List<PRonda3> jugadoresConCartas}) {
    datosJugadoresr3 = jugadoresConCartas;
  }

  @override
  Future<String> registrarPuntuacionFinal() async {
    for (var datos in datosJugadoresr1) {
      dtsJugadoresr1.add(
          PRonda1(jugador: datos.jugador, cuantasAzules: datos.cuantasAzules));
    }

    for (var i = 0; i < dtsJugadoresr1.length; i++) {
      dtsJugadoresr2.add(PRonda2(
          jugador: datosJugadoresr2[i].jugador,
          cuantasAzules: datosJugadoresr2[i].cuantasAzules +
              datosJugadoresr1[i].cuantasAzules,
          cuantasVerdes: datosJugadoresr2[i].cuantasVerdes +
              datosJugadoresr1[i].cuantasVerdes));
    }

    for (var i = 0; i < dtsJugadoresr2.length; i++) {
      dtsJugadoresr3.add(PRonda3(
          jugador: datosJugadoresr3[i].jugador,
          cuantasAzules: datosJugadoresr3[i].cuantasAzules +
              datosJugadoresr2[i].cuantasAzules +
              datosJugadoresr1[i].cuantasAzules,
          cuantasVerdes: datosJugadoresr3[i].cuantasVerdes +
              datosJugadoresr2[i].cuantasVerdes +
              datosJugadoresr1[i].cuantasVerdes,
          cuantasNegras: datosJugadoresr3[i].cuantasNegras +
              datosJugadoresr2[i].cuantasNegras +
              datosJugadoresr1[i].cuantasNegras,
          cuantasRosas: datosJugadoresr3[i].cuantasRosas +
              datosJugadoresr2[i].cuantasRosas +
              datosJugadoresr1[i].cuantasRosas));
    }

    _jugadores = dtsJugadoresr1.map((e) => e.jugador).toSet();
    partida = Partida(jugadores: _jugadores);

    try {
      partida.puntuacionRonda1(dtsJugadoresr1);
      partida.puntuacionRonda2(dtsJugadoresr2);
      partida.puntuacionRonda3(dtsJugadoresr3);

      puntuacionesJugadorFinal = partida.puntuaciones(FasePuntuacion.desenlace)
          as List<PuntuacionJugadorFinal>;

      return 'completado';
    } on ProblemaOrdenIncorrecto {
      return 'ordenincorrecto';
    } on ProblemaJugadoresNoConcuerdan {
      return 'jugadoresnoconcuerdan';
    } on ProblemaDisminucionAzules {
      return 'ProblemaDisminucionAzules';
    } on ProblemaDisminucionVerdes {
      return 'ProblemaDisminucionVerdes';
    } catch (e) {
      return 'error';
    }
  }

  @override
  Future<String> subirPartida({required String usuario}) async {
    for (var i = 0; i < datosJugadoresr1.length; i++) {
      detallesJugador.add(DetallesJugador(
          nombreJugador: datosJugadoresr1[i].jugador.nombre,
          puntuacionFinal: puntuacionesJugadorFinal[i].total,
          ronda1: Cartas(
              azules: datosJugadoresr1[i].cuantasAzules,
              verdes: datosJugadoresr1[i].cuantasVerdes,
              negras: datosJugadoresr1[i].cuantasNegras,
              rosas: datosJugadoresr1[i].cuantasRosas),
          ronda2: Cartas(
              azules: datosJugadoresr2[i].cuantasAzules,
              verdes: datosJugadoresr2[i].cuantasVerdes,
              negras: datosJugadoresr2[i].cuantasNegras,
              rosas: datosJugadoresr2[i].cuantasRosas),
          ronda3: Cartas(
              azules: datosJugadoresr3[i].cuantasAzules,
              verdes: datosJugadoresr3[i].cuantasVerdes,
              negras: datosJugadoresr3[i].cuantasNegras,
              rosas: datosJugadoresr3[i].cuantasRosas)));
    }

    DateTime now = DateTime.now();
    DateFormat formatear = DateFormat('yyyy-MM-dd');
    String fecha = formatear.format(now);

    PartidasJugador subirPartida =
        PartidasJugador(fecha: fecha, jugadores: detallesJugador);

    try {
      var db = await Db.create(conexionServidor);

      await db.open();

      var col = db.collection('usuarios');

      await col.update({
        "nombre": usuario,
      }, {
        "\$push": {"partidas": jsonDecode(subirPartida.toJson())}
      });

      return partidaSubida;
    } catch (e) {
      return partidaErrorConexion;
    }
  }

  @override
  Future<String> descargarData({required String nombre}) async {
    try {
      var db = await Db.create(conexionServidor);

      await db.open();

      var col = db.collection('usuarios');

      var data = await col.findOne(where.eq('nombre', nombre));

      if (data != null) {
        dataUsuario = data as Map<String, dynamic>;
        return dataDescargada;
      }

      return dataVacia;
    } catch (e) {
      return descargaErrorConexion;
    }
  }

  @override
  Map<String, dynamic> obtenerPartidas() {
    return dataUsuario;
  }

  @override
  void cerrarSesion() {
    _nombresJugadores = [];
    _jugadores = {};
    datosJugadoresr1 = [];
    datosJugadoresr2 = [];
    datosJugadoresr3 = [];

    dtsJugadoresr1 = [];
    dtsJugadoresr2 = [];
    dtsJugadoresr3 = [];

    puntuacionesJugadorFinal = [];
    detallesJugador = [];
    cartasRonda1 = [];
    dataUsuario = {};
  }

  @override
  Future<String> eliminarPartida(
      {required int indice, required String usuario}) async {
    Map<String, dynamic> infoUsuario = obtenerPartidas();

    try {
      var db = await Db.create(conexionServidor);

      await db.open();

      var col = db.collection('usuarios');

      await col.update({
        "nombre": usuario,
      }, {
        "\$pull": {"partidas": infoUsuario['partidas'][indice]}
      });

      String dataActualizada = await descargarData(nombre: usuario);

      if (dataActualizada == dataDescargada) {
        return partidaEliminada;
      } else {
        return eliminarErrorConexion;
      }
    } catch (e) {
      return eliminarErrorConexion;
    }
  }
}
